package nst_0x02_threaded;

import java.net.*;
import java.io.*;

class my_thread extends Thread{
	InputStream is;
	public my_thread(InputStream i){
		is = i;
	}
	public void run() {
		try {
			int ch;
			while((ch = is.read()) != -1)
				System.out.write(ch);
			System.out.println("* server said ktxbye");
			System.exit(0);
		} catch (Exception e) {
			if(e instanceof SocketException) {
				System.out.println("* we're at EOF -> the thread is dead");
			}
			else e.printStackTrace();
		}
	}
}
public class Main {
	public static void main(String[] args) {
		String host = "smtp.utb.cz";
		int port = 25;
		if(args.length > 0) {
			host = args[0];
			if(args.length >1) {
				port = Integer.parseInt(args[1]);
			}
		}
		try {
			Socket s = new Socket(host,port);
			InputStream is = s.getInputStream();
			OutputStream os = s.getOutputStream();
			my_thread mt = new my_thread(is);
			mt.start();
			int ch;
			while((ch = System.in.read())!=-1) {
				os.write(ch);
			}
			System.out.println("* we're at EOF");
			s.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

